<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Register</title>
        <link rel="stylesheet" type="text/css" href="style.css" media="all"></link>

    </head>
    <body class="reg1">
         <!--display the user a registration form-->
        <div class="login-page">
            <div class="form">
                <form action="MazeRegister" class="register-form"  method="post">
                    <input type="text" name="username" value="<% if(request.getAttribute("username") != null){
                        out.println(request.getAttribute("username"));} %>" placeholder="Username"/>
                    <input type="password" name="password" placeholder="Password"/>
                    <input type="text" name="name" value="<% if(request.getAttribute("name") != null){
                        out.println(request.getAttribute("name"));} %>" placeholder="Name"/>
                    <input type="text" name="email" value="<% if(request.getAttribute("email") != null){
                        out.println(request.getAttribute("email"));} %>" placeholder="Email Address"/>
                    <!--display a selection of images for the user to choose-->
                    <div class="icon">
                        <img src="Images/female1.png" height=40px width =40px/>
                        <input type="radio" name="photo" value="female1" checked="checked"/>
                    </div>
                    <div class="icon">
                        <img src="Images/female2.png" height=40px width =40px/>
                        <input type="radio" name="photo" value="female2"/>
                    </div>
                    <div class="icon">
                        <img src="Images/male1.png" height=40px width =40px/>
                    <input type="radio" name="photo" value="male1"/>
                    
                    </div>
                    <div class="icon">
                        <img src="Images/male2.png" height=40px width =40px/>
                        <input type="radio" name="photo" value="male2"/>                 
                    </div>
                    <div class="icon">
                        <img src="Images/clown.png" height=40px width =40px/>
                        <input type="radio" name="photo" value="clown"/>                  
                    </div>
                    <!--display a reser button-->
                    <button type="reset">Reset</button>
                    <p/>
                    
                    <button type="submit">create</button>
                    <!--if one of the fields is missing when pressing create button, diplay error-->
                    <% if (request.getAttribute("error") != null
                                && (Boolean) request.getAttribute("error")) {%>
                                <% if( request.getAttribute("username") != null)
                                request.getParameter("username");%>
                    <p class="message error"><error href="#">Missing fields. Please try again </error></p>
                            <% }%>
                    <!--if the username is occupied when pressing login button, diplay error-->
                            <% if (request.getAttribute("exist") != null
                                        && (Boolean) request.getAttribute("exist")) {%>
                    <p class="message error"><error href="#">This username is occupied. Please choose another username.</error></p>
                            <% }%>
                     <!--suggest the user to go to login form-->
                    <p class="message">Already registered? <a href="login.jsp">Sign In</a></p>
                </form>
            </div>
        </div>
    </body>
</html>