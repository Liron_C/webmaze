<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="style.css" media="all"/>

    </head>
    <!--display a login form to the user-->
    <body class="log1">
        <div class="login-page">
            <div class="form">
                <form action="MazeLogin" method="post">
                    <input type="text" name="username" placeholder="username"/>
                    <input type="password" name="password" placeholder="password"/>
                    <button type="submit">login</button>
                    <!--if one of the field is wrong when pressing login button, diplay error-->
                    <% if (request.getAttribute("error") != null
                                && (Boolean) request.getAttribute("error")) {%>
                    <p class="message error"><error href="#">Wrong username/password. Please try again </error></p>
                            <% }%>
                    <!--suggest the user to go to registration form-->
                    <p class="message">Not registered? <a href="register.jsp">Create an account</a></p>
                </form>
            </div>
        </div>
    </body>
</html>