var myCurrentRow;
var myCurrentCol;
var otherCurrentRow;
var otherCurrentCol;
var myStartRow;
var myStartCol;
var myEndRow;
var myEndCol;
var otherStartRow;
var otherStartCol;
var otherEndRow;
var otherEndCol;
var wasHint;
var hintRow;
var hintCol;
var mazeString;
var height;
var width;
var myPhoto;
var otherPhoto;
var mazeSolution;
var move;
/*
 * the function reset the table of the board
 */
function reset() {
    // get the reference of the form
    var form = document.getElementsByTagName("form")[0];
    // creates a table element ans tbody element
    var tbl1 = document.createElement("table1");
    var tblBody1 = document.createElement("tbody1");
    // creating the cells
    for (var i = 0; i < 2 * height - 1; i++) {
        // creates a row
        var row = document.createElement("tr");
        for (var j = 0; j < 2 * width - 1; j++) {
            //creates a col
            var cell = document.createElement("td");
            cell.style.width = '20px';
            cell.style.height = '20px';
            cell.id = "id" + i.toString() + "," + j.toString();
            row.appendChild(cell);
        }

        // add the row to the table
        tblBody1.appendChild(row);
    }
    tbl1.appendChild(tblBody1);
    form.appendChild(tbl1);
    //set the border to 0 and move the table to the center
    tbl1.setAttribute("border", "0");
    tbl1.style.borderSpacing = "0px";
    $("table1").attr("align", "left");

    // get the reference of the form
    var form = document.getElementsByTagName("form")[0];

    // creates a table element ans tbody element
    var tbl2 = document.createElement("table2");
    var tblBody2 = document.createElement("tbody2");

    // creating the cells
    for (var i = 0; i < 2 * height - 1; i++) {
        // creates a row
        var row = document.createElement("tr");

        for (var j = 0; j < 2 * width - 1; j++) {
            //creates a col
            var cell = document.createElement("td");
            cell.style.width = '20px';
            cell.style.height = '20px';

            cell.id = "idd" + i.toString() + "," + j.toString();
            row.appendChild(cell);
        }

        // add the row to the table
        tblBody2.appendChild(row);
    }

    tbl2.appendChild(tblBody2);
    form.appendChild(tbl2);
    //set the border to 0 and move the table to the center
    tbl2.setAttribute("border", "0");
    tbl2.style.borderSpacing = "0px";

    tbl1.style["margin-right"] = "70px";
    $("table2").attr("align", "right");
}

/*
 * the long poling function
 */
$(function ($)
{
    //long poling that gets the data from the get function in the GetAnsToMulti servlet
    function long_polling()
    {
        $.getJSON('GetAnsToMulti', function (data)
        {
            //check if it solution answer
            if (data.Type === 2)
            {
                mazeSolution = data.Content.Maze;
                hint();
            }
            //check if it new multiplayer game
            if (data.Type === 3)
            {
                //the sizes of the board
                height = data.Height;
                width = data.Width;
                //call to the reset function
                reset();
                //hide the main element
                document.getElementById("main").style.visibility = "hidden";
                wasHint = 0;
                //gets the start and the end values from the data and updates the variables accordingly
                myStartRow = 2 * data.Content.You.Start.Row;
                myStartCol = 2 * data.Content.You.Start.Col;
                myCurrentRow = myStartRow;
                myCurrentCol = myStartCol;
                myEndRow = 2 * data.Content.You.End.Row;
                myEndCol = 2 * data.Content.You.End.Col;
                otherStartRow = myEndRow;
                otherStartCol = myEndCol;
                otherCurrentRow = otherStartRow;
                otherCurrentCol = otherStartCol;
                otherEndRow = myStartRow;
                otherEndCol = myStartCol;
                myStartLocation = "id" + myStartRow.toString() + "," + myStartCol.toString();
                myEndLocation = "id" + myEndRow.toString() + "," + myEndCol.toString();
                otherStartLocation = "idd" + otherStartRow.toString() + "," + otherStartCol.toString();
                otherEndLocation = "idd" + otherEndRow.toString() + "," + otherEndCol.toString();

                //gets the photoes of the users
                myPhoto = data.MyPhoto;
                otherPhoto = data.OtherPhoto;
                //update the board by the maze that we get
                var count = 0;
                mazeString = data.Content.You.Maze;
                for (i = 0; i < height * 2 - 1; i++)
                {
                    for (j = 0; j < width * 2 - 1; j++)
                    {
                        var location1 = "id" + i.toString() + "," + j.toString();
                        var location2 = "idd" + i.toString() + "," + j.toString();
                        //check if the char is "0"
                        if (mazeString[count] === '0')
                        {
                            document.getElementById(location1).style.background = "blue";
                            document.getElementById(location2).style.background = "blue";
                            //check if the char is "1"
                        } else {
                            document.getElementById(location1).style.background = "black";
                            document.getElementById(location2).style.background = "black";
                        }
                        count++;
                    }
                }
                var myPlace = "url('" + myPhoto + "')";
                var otherPlace = "url('" + otherPhoto + "')";
                //puts the start and the end photoes on the board
                document.getElementById(myStartLocation).style.backgroundImage = myPlace;
                document.getElementById(myStartLocation).style.backgroundSize = "cover";
                document.getElementById(otherStartLocation).style.backgroundImage = otherPlace;
                document.getElementById(otherStartLocation).style.backgroundSize = "cover";
                document.getElementById(myEndLocation).style.backgroundImage = "url('Images/endPic.png')";
                document.getElementById(myEndLocation).style.backgroundSize = "cover";
                document.getElementById(otherEndLocation).style.backgroundImage = "url('Images/endPic.png')";
                document.getElementById(otherEndLocation).style.backgroundSize = "cover";
            }
            //check if it move message
            else if (data.Type === 4)
            {
                move = data.Content.Move;
                getMoveFromOther();
            }
            long_polling();
        });
    }
    //call to the long poling function
    long_polling();
});

/*
 * the key press function- move the user by his press
 */
$(document).keydown(function (e) {
    //if there was a hint on the board remove it
    if (wasHint === 1)
    {
        var location = "id" + hintRow.toString() + "," + hintCol.toString();
        document.getElementById(location).style.background = "blue";
        wasHint = 0;
    }
    //if the user press left
    if (e.keyCode === 37)
    {
        //check that the move is valid
        if ((myCurrentCol !== 0) && mazeString[(2 * width - 1) * myCurrentRow + myCurrentCol - 1] === '0')
        {
            var location = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            document.getElementById(location).style.background = "blue";
            myCurrentCol--;
            move = "left";
            updateOtherByMove();
            //check if the user is win
            if (myCurrentRow === myEndRow && myCurrentCol === myEndCol)
            {
                alert("You Win!");
                closeGame();
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            var place = "url('" + myPhoto + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";

        }
//if the user press right
    } else if (e.keyCode === 39) {
        //check that the move is valid
        if ((myCurrentCol !== (2 * width - 2)) && mazeString[(2 * width - 1) * myCurrentRow + myCurrentCol + 1] === '0')
        {
            var location = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            document.getElementById(location).style.background = "blue";
            myCurrentCol++;
            move = "right";
            updateOtherByMove();
            //check if the user is win
            if (myCurrentRow === myEndRow && myCurrentCol === myEndCol)
            {
                alert("You Win!");
                closeGame();
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            var place = "url('" + myPhoto + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";
        }
//if the user press up
    } else if (e.keyCode === 38) {
        //check that the move is valid
        if ((myCurrentRow !== 0) && mazeString[(2 * width - 1) * (myCurrentRow - 1) + myCurrentCol] === '0')
        {
            var location = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            document.getElementById(location).style.background = "blue";
            myCurrentRow--;
            move = "up";
            updateOtherByMove();
            //check if the user is win
            if (myCurrentRow === myEndRow && myCurrentCol === myEndCol)
            {
                alert("You Win!");
                closeGame();
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            var place = "url('" + myPhoto + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";

        }
        //if the user press down
    } else if (e.keyCode === 40) {
        //check that the move is valid
        if ((myCurrentRow !== (2 * height - 2)) && mazeString[(2 * width - 1) * (myCurrentRow + 1) + myCurrentCol] === '0')
        {
            var location = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            document.getElementById(location).style.background = "blue";
            myCurrentRow++;
            move = "down";
            updateOtherByMove();
            //check if the user is win
            if (myCurrentRow === myEndRow && myCurrentCol === myEndCol)
            {
                alert("You Win!");
                closeGame();
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + myCurrentRow.toString() + "," + myCurrentCol.toString();
            var place = "url('" + myPhoto + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";
        }
    }
});

/*
 * function sureRestart play confirm window and check if the player sure about the action
 * go the player to the main menu
 */
function sureRestart() {
    var answer = confirm("Are you sure?");
    if (answer) {
        closeGame();
        window.location = "menu.jsp";
    }
}

/*
 * function sureRestart play confirm window and check if the player sure about the action
 * go the player to the main menu
 */
function sureMenu() {
    var answer = confirm("Are you sure?");
    if (answer) {
        closeGame();
        window.location = "menu.jsp";
    }
}

/*
 * function getHint send the current place to the servlet and update the place of the hint on the board
 */
function getHint()
{
    $.post('SendHint',
            {
                curR: myCurrentRow,
                curC: myCurrentCol,
                endR: myEndRow,
                endC: myEndCol
            });
}

/*
 * function that update the hint on the board
 */
function hint()
{
    //check if there is hint for up
    if ((myCurrentRow !== 0) && mazeSolution[(2 * width - 1) * (myCurrentRow - 1) + myCurrentCol] === '2')
    {
        hintRow = myCurrentRow - 1;
        hintCol = myCurrentCol;
        var location = "id" + hintRow.toString() + "," + hintCol.toString();
        document.getElementById(location).style.background = "red";
    }
    //check if there is hint for down
    else if ((myCurrentRow !== (2 * height - 2)) && mazeSolution[(2 * width - 1) * (myCurrentRow + 1) + myCurrentCol] === '2')
    {
        hintRow = myCurrentRow + 1;
        hintCol = myCurrentCol;
        var location = "id" + hintRow.toString() + "," + hintCol.toString();
        //paint the hint by red
        document.getElementById(location).style.background = "red";
    }
    //check if there is hint for right
    else if ((myCurrentCol !== (2 * width - 2)) && mazeSolution[(2 * width - 1) * myCurrentRow + myCurrentCol + 1] === '2')
    {
        hintRow = myCurrentRow;
        hintCol = myCurrentCol + 1;
        var location = "id" + hintRow.toString() + "," + hintCol.toString();
        //paint the hint by red
        document.getElementById(location).style.background = "red";
    }
    //check if there is hint for left
    else if ((myCurrentCol !== 0) && mazeSolution[(2 * width - 1) * myCurrentRow + myCurrentCol - 1] === '2')
    {
        hintRow = myCurrentRow;
        hintCol = myCurrentCol - 1;
        var location = "id" + hintRow.toString() + "," + hintCol.toString();
        //paint the hint by red
        document.getElementById(location).style.background = "red";
    }
    //change the flag of the hint to 1
    wasHint = 1;
}

/*
 * this function update the place of the other player on the board
 */
function updateOtherByMove()
{
    //send to the SendMove servlet the move
    $.post('SendMove',
            {
                move: move
            });
}

/*
 * this class send the close message to the server and close the game
 */
function closeGame()
{
    $.post('SendClose');
}

/*
 * this function change the place of the other player on the board
 */
function getMoveFromOther()
{

//if the user press left
    if (move === "left")
    {
        var location = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        document.getElementById(location).style.background = "blue";
        otherCurrentCol--;
        //check if the user is lose
        if (otherCurrentRow === otherEndRow && otherCurrentCol === otherEndCol)
        {
            alert("You Lose!");
            closeGame();
            window.location = "menu.jsp";
        }
        //put the player on the new place
        var newLocation = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        var place = "url('" + otherPhoto + "')";
        document.getElementById(newLocation).style.backgroundImage = place;
        document.getElementById(newLocation).style.backgroundSize = "cover";
    }
    //if the user press right
    else if (move === "right")
    {
        var location = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        document.getElementById(location).style.background = "blue";
        otherCurrentCol++;
        //check if the user is lose
        if (otherCurrentRow === otherEndRow && otherCurrentCol === otherEndCol)
        {
            alert("You Lose!");
            closeGame();
            window.location = "menu.jsp";
        }
        //put the player on the new place
        var newLocation = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        var place = "url('" + otherPhoto + "')";
        document.getElementById(newLocation).style.backgroundImage = place;
        document.getElementById(newLocation).style.backgroundSize = "cover";

//if the user press up
    } else if (move === "up")
    {
        var location = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        document.getElementById(location).style.background = "blue";
        otherCurrentRow--;
        //check if the user is lose
        if (otherCurrentRow === otherEndRow && otherCurrentCol === otherEndCol)
        {
            alert("You Lose!");
            closeGame();
            window.location = "menu.jsp";
        }
        //put the player on the new place
        var newLocation = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        var place = "url('" + otherPhoto + "')";
        document.getElementById(newLocation).style.backgroundImage = place;
        document.getElementById(newLocation).style.backgroundSize = "cover";
//if the user press down
    } else if (move === "down")
    {
        var location = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        document.getElementById(location).style.background = "blue";
        otherCurrentRow++;
        //check if the user is lose
        if (otherCurrentRow === otherEndRow && otherCurrentCol === otherEndCol)
        {
            alert("You Lose!");
            closeGame();
            window.location = "menu.jsp";
        }
        //put the player on the new place
        var newLocation = "idd" + otherCurrentRow.toString() + "," + otherCurrentCol.toString();
        var place = "url('" + otherPhoto + "')";
        document.getElementById(newLocation).style.backgroundImage = place;
        document.getElementById(newLocation).style.backgroundSize = "cover";
    }
}