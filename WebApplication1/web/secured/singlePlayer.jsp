<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="singlePlayer.css" media="all"></link>
<!DOCTYPE html>
<html>
    <!--the main screen of the multiplayer game-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Single Player game</title>
        <link rel="stylesheet" type="text/css" href="singlePlayerStyle.css" media="all"/>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <script src="longPoling.js"></script>
    </head>
    <body class="single1">
        <div class ="center">
            <div class="form">
                <!--display 3 buttons for user's choise-->
                <div class="buttons">
                    <button class="button" onclick="getHint()" name="game" value="hint">Hint</button>
                    <button class="button" onclick="sureRestart()" name="game" value ="restart">Restart</button>
                    <button class="button" onclick="sureMenu()" name="game" value ="menu">Menu</button>
                    <br><br><br>
                </div>
                <!--go to "doPost" while pressing one of the buttons-->
                <form action="SinglePlayer" method="post">
                    <!--display user's details on the right side of the screen-->
                    <div class="user">
                        <div>
                            <img name="photo" src=${photo} height=40px width =40px/>
                            <p name="username">Hello ${name}!</p>
                        </div>
                        <form action="SinglePlayer" method="post">
                            <button type="submit" name="game" value="logout" class="buttonOut">Logout</button>
                        </form>
                    </div>
                </form>
            </div>
        </div>
        <!--create a loading animation while the screen information is loading-->
        <div id="main">
            <div class="small1">
                <div class="small ball smallball1"></div>
                <div class="small ball smallball2"></div>
                <div class="small ball smallball3"></div>
                <div class="small ball smallball4"></div>
            </div>
            <div class="small2">
                <div class="small ball smallball5"></div>
                <div class="small ball smallball6"></div>
                <div class="small ball smallball7"></div>
                <div class="small ball smallball8"></div>
            </div>
            <div class="bigcon">
                <div class="big ball"></div>
            </div>
        </div>
    </body>
</html>
