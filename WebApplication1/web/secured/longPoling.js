var currentPlaceRow;
var currentPlaceCol;
var startPlaceRow;
var startPlaceCol;
var endPlaceRow;
var endPlaceCol;
var wasHint;
var hintRow;
var hintCol;
var mazeString;
var height;
var width;
var photo;
/*
 * the function reset the table of the board
 */
function reset() {
    // get the reference of the form
    var form = document.getElementsByTagName("form")[0];

    // creates a table element ans tbody element
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");

    // creating the cells
    for (var i = 0; i < 2 * height; i++) {
        // creates a row
        var row = document.createElement("tr");
        for (var j = 0; j < 2 * width - 1; j++) {
            //creates a col
            var cell = document.createElement("td");
            cell.style.width = '20px';
            cell.style.height = '20px';
            cell.id = "id" + i.toString() + "," + j.toString();
            row.appendChild(cell);
        }
        // add the row to the table
        tblBody.appendChild(row);
    }
    tbl.appendChild(tblBody);
    form.appendChild(tbl);
    //set the border to 0 and move the table to the center
    tbl.setAttribute("border", "0");
    tbl.style.borderSpacing = "0px";
    $("table").attr("align", "center");
}

/*
 * the long poling function
 */
$(function ($)
{
    function long_polling()
    {
        //long poling that gets the data from the get function in the GetJson servlet
        $.getJSON('GetJson', function (data)
        {
            //the sizes of the board
            height = data.Height;
            width = data.Width;
            //call to the reset function
            reset();
            //hide the main element
            document.getElementById("main").style.visibility = "hidden";
            wasHint = 0;
            //gets the start and the end values from the data and updates the variables accordingly
            var startRow = 2 * data.Content.Start.Row;
            var startCol = 2 * data.Content.Start.Col;
            var startLocation = "id" + startRow.toString() + "," + startCol.toString();
            startPlaceRow = startRow;
            startPlaceCol = startCol;
            currentPlaceRow = startRow;
            currentPlaceCol = startCol;
            var endRow = 2 * data.Content.End.Row;
            var endCol = 2 * data.Content.End.Col;
            endPlaceRow = endRow;
            endPlaceCol = endCol;
            //gets the photo of the user
            photo = data.Photo;
            var endLocation = "id" + endRow.toString() + "," + endCol.toString();
            //update the board by the maze that we get
            var count = 0;
            mazeString = data.Content.Maze;
            for (i = 0; i < height * 2 - 1; i++)
            {
                for (j = 0; j < width * 2 - 1; j++)
                {
                    var location = "id" + i.toString() + "," + j.toString();
                    //check if the char is "0"
                    if (mazeString[count] === '0')
                    {
                        document.getElementById(location).style.background = "blue";
                        //check if the char is "1"
                    } else {
                        document.getElementById(location).style.background = "black";
                    }
                    count++;
                }
            }
            var place = "url('" + photo + "')";
            //puts the start and the end photoes on the board
            document.getElementById(startLocation).style.backgroundImage = place;
            document.getElementById(startLocation).style.backgroundSize = "cover";
            document.getElementById(endLocation).style.backgroundImage = "url('Images/endPic.png')";
            document.getElementById(endLocation).style.backgroundSize = "cover";
        });
    }
    //call to the long poling function
    long_polling();
});

/*
 * the key press function- move the user by his press
 */
$(document).keydown(function (e) {
    //if there was a hint on the board remove it
    if (wasHint === 1)
    {
        var location = "id" + hintRow.toString() + "," + hintCol.toString();
        document.getElementById(location).style.background = "blue";
        wasHint = 0;
    }
    //if the user press left
    if (e.keyCode === 37) {
        //check that the move is valid
        if ((currentPlaceCol !== 0) && mazeString[(2 * width - 1) * currentPlaceRow + currentPlaceCol - 1] === '0')
        {
            var location = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            document.getElementById(location).style.background = "blue";
            currentPlaceCol--;
            //check if the user is win
            if (currentPlaceRow === endPlaceRow && currentPlaceCol === endPlaceCol)
            {
                alert("You Win!");
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            var place = "url('" + photo + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";
        }
        //if the user press right
    } else if (e.keyCode === 39) {
        //check that the move is valid
        if ((currentPlaceCol !== (2 * width - 2)) && mazeString[(2 * width - 1) * currentPlaceRow + currentPlaceCol + 1] === '0')
        {
            var location = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            document.getElementById(location).style.background = "blue";
            currentPlaceCol++;
            //check if the user is win
            if (currentPlaceRow === endPlaceRow && currentPlaceCol === endPlaceCol)
            {
                alert("You Win!");
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            var place = "url('" + photo + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";
        }
        //if the user press up
    } else if (e.keyCode === 38) {
        //check that the move is valid
        if ((currentPlaceRow !== 0) && mazeString[(2 * width - 1) * (currentPlaceRow - 1) + currentPlaceCol] === '0')
        {
            var location = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            document.getElementById(location).style.background = "blue";
            currentPlaceRow--;
            //check if the user is win
            if (currentPlaceRow === endPlaceRow && currentPlaceCol === endPlaceCol)
            {
                alert("You Win!");
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            var place = "url('" + photo + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";
        }
        //if the user press down
    } else if (e.keyCode === 40) {
        //check that the move is valid
        if ((currentPlaceRow !== (2 * height - 2)) && mazeString[(2 * width - 1) * (currentPlaceRow + 1) + currentPlaceCol] === '0')
        {
            var location = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            document.getElementById(location).style.background = "blue";
            currentPlaceRow++;
            //check if the user is win
            if (currentPlaceRow === endPlaceRow && currentPlaceCol === endPlaceCol)
            {
                alert("You Win!");
                window.location = "menu.jsp";
            }
            //put the player on the new place
            var newLocation = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
            var place = "url('" + photo + "')";
            document.getElementById(newLocation).style.backgroundImage = place;
            document.getElementById(newLocation).style.backgroundSize = "cover";
        }
    }
});

/*
 * restart the game - move the player to the start position
 */
function restart() {
    //if there was a hint on the board remove it
    if (wasHint === 1)
    {
        var location = "id" + hintRow.toString() + "," + hintCol.toString();
        document.getElementById(location).style.background = "blue";
        wasHint = 0;
    }
    //remove the current position of the player
    var location = "id" + currentPlaceRow.toString() + "," + currentPlaceCol.toString();
    document.getElementById(location).style.background = "blue";
    //move the player to the start position
    var startLocation = "id" + startPlaceRow.toString() + "," + startPlaceCol.toString();
    var place = "url('" + photo + "')";
    document.getElementById(startLocation).style.backgroundImage = place;
    document.getElementById(startLocation).style.backgroundSize = "cover";
    //update the current places
    currentPlaceRow = startPlaceRow;
    currentPlaceCol = startPlaceCol;
}

/*
 * function sureRestart play confirm window and check if the player sure about the action
 */
function sureRestart() {
    var answer = confirm("Are you sure?");
    if (answer)
        restart();
}

/*
 * function sureRestart play confirm window and check if the player sure about the action
 * go the player to the main menu
 */
function sureMenu() {
    var answer = confirm("Are you sure?");
    if (answer)
        window.location = "menu.jsp";
}

/*
 * function getHint send the current place to the servlet and update the place of the hint on the board
 */
function getHint()
{
    $.get('GetSolution',
            {
                curR: currentPlaceRow,
                curC: currentPlaceCol,
                endR: endPlaceRow,
                endC: endPlaceCol
            },
    //function that update the hint on the board
            function (data)
            {
                //get the maze with the solution
                var maze = data.Content.Maze;
                //check if there is hint for up
                if ((currentPlaceRow !== 0) && maze[(2 * width - 1) * (currentPlaceRow - 1) + currentPlaceCol] === '2')
                {
                    hintRow = currentPlaceRow - 1;
                    hintCol = currentPlaceCol;
                    var location = "id" + hintRow.toString() + "," + hintCol.toString();
                    //paint the hint by red
                    document.getElementById(location).style.background = "red";
                }
                //check if there is hint for down
                else if ((currentPlaceRow !== (2 * height - 2)) && maze[(2 * width - 1) * (currentPlaceRow + 1) + currentPlaceCol] === '2')
                {
                    hintRow = currentPlaceRow + 1;
                    hintCol = currentPlaceCol;
                    var location = "id" + hintRow.toString() + "," + hintCol.toString();
                    //paint the hint by red
                    document.getElementById(location).style.background = "red";
                }
                //check if there is hint for right
                else if ((currentPlaceCol !== (2 * width - 2)) && maze[(2 * width - 1) * currentPlaceRow + currentPlaceCol + 1] === '2')
                {
                    hintRow = currentPlaceRow;
                    hintCol = currentPlaceCol + 1;
                    var location = "id" + hintRow.toString() + "," + hintCol.toString();
                    //paint the hint by red
                    document.getElementById(location).style.background = "red";
                }
                //check if there is hint for left
                else if ((currentPlaceCol !== 0) && mazeString[(2 * width - 1) * currentPlaceRow + currentPlaceCol - 1] === '2')
                {
                    hintRow = currentPlaceRow;
                    hintCol = currentPlaceCol - 1;
                    var location = "id" + hintRow.toString() + "," + hintCol.toString();
                    //paint the hint by red
                    document.getElementById(location).style.background = "red";
                }
                //change the flag of the hint to 1
                wasHint = 1;
            });
}