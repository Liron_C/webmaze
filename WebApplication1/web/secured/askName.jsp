<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <!-- the head of the page-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Maze Name</title>
        <link rel="stylesheet" type="text/css" href="name.css" media="all"/>
    </head>
    <!--the body of the page-->
    <body class="log1">
        <!--creates the page-->
        <div class="login-page">
            <div class="form">
                <!-- the ask name window-->
                <form action="AskName" method="post">
                    <p class="message">Choose maze name:</p>
                    <p></p>
                    <input type="text" name="mazeName" placeholder="mazeName"/>
                    <!-- button of starting the game-->
                    <button type="submit">Start</button>
                </form>
            </div>
        </div>
    </body>
</html>
