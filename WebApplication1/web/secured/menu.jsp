<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <!-- the head of the page-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Maze Menu</title>
        <link rel="stylesheet" type="text/css" href="menuStyle.css" media="all"/>
    </head>
    <!--the body of the page-->
    <body class="menu1">
        <!-- creates a menu object-->
        <div class="allPage">
            <!--the user box-->
            <div class="user">
                <div>
                    <img name="photo" src=${photo} height=40px width =40px/>
                    <P name="username">Hello ${name}!</P>
                </div>
                <form action="MazeMenu" method="post">
                    <button type="submit" name="game" value="logout" class="buttonLogout">Logout</button>
                </form>
            </div>
            <!--the menu of the game-->
            <div class="page">
                <img class="logo" name="photo" src="Images/logo.png"/>
                <div class="form">
                    <form action="MazeMenu" method="post">
                        <!--the dingle button-->
                        <button type="submit" name="game" value="single">Single Player Game</button>
                        <!-- the multiplayer button-->
                        <button type="submit" name="game" value ="multi">Multiplayer Game</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>