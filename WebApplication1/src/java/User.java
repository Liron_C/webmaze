
/**
 * This is the main class to hold clients information.
 */
public class User {

    private String username;
    private String password;
    private String name;
    private String e_mail;
    private String photo;
    //game members
    private String maze;
    private Connection connection;

    /**
     * class constructor.
     *
     * @param user
     * @param pass
     * @param name
     * @param mail
     * @param photo
     * @param port
     * @param ip
     */
    public User(String user, String pass, String name, String mail, String photo, int port, String ip) {
        this.username = user;
        this.password = pass;
        this.name = name;
        this.e_mail = mail;
        this.photo = photo;
        this.maze = "";
        this.connection = new Connection(port, ip);
    }

    /**
     * get user's username
     *
     * @return username
     */
    public String GetUser() {
        return this.username;
    }

    /**
     * get user's password
     *
     * @return password
     */
    public String GetPassword() {
        return this.password;
    }

    /**
     * get user's name
     *
     * @return name
     */
    public String GetName() {
        return this.name;
    }

    /**
     * get user's email
     *
     * @return email
     */
    public String GetMail() {
        return this.e_mail;
    }

    /**
     * get user's photo
     *
     * @return the string of the path of the photo
     */
    public String GetPhoto() {
        if (this.photo.equals("male1")) {
            return "Images/male1.png";
        } else if (this.photo.equals("male2")) {
            return "Images/male2.png";
        } else if (this.photo.equals("female1")) {
            return "Images/female1.png";
        } else if (this.photo.equals("female2")) {
            return "Images/female2.png";
        } else {
            return "Images/clown.png";
        }
    }

    /**
     * get user's maze
     *
     * @return string maze
     */
    public String getMaze() {
        return this.maze;
    }

    /**
     * set user's maze
     *
     * @param maze
     */
    public void setMaze(String maze) {
        this.maze = maze;
    }

    /**
     * send a user's message to the server
     *
     * @param message
     */
    public void sentToServer(String message) {
        this.connection.sentToServer(message);
    }

    /**
     * get a user's message from the server
     *
     * @return
     */
    public String receiveFromServer() {
        return this.connection.receiveFromServer();
    }
}
