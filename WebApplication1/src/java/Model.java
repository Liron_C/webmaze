
/**
 * class Model transfer the information of the users
 * singelton class
 */
public class Model {

    private static Model instance = null;
    private UsersData users;

    /**
     * the constructor of the Model
     */
    protected Model() {
        this.users = UsersData.GetInstance();
    }

    /**
     * function GetInstance - create newModel if not exist
     */
    public static Model GetInstance() {
        if (instance == null) {
            instance = new Model();
        }
        return instance;
    }

    /**
     * getUser function
     *
     * @param username the username
     * @return the username of the player
     */
    public User getUser(String username) {
        return this.users.getUser(username);
    }

    /**
     * getMultiUser function
     *
     * @param name the name of the player
     * @return the User info
     */
    public User getMultiUser(String name) {
        return this.users.getMultiUser(name);
    }

    /**
     * check if user exist in the hash map
     *
     * @param name the name of the user
     * @return true if exist, else- false
     */
    public boolean checkUser(String name) {
        return this.users.IfContains(name);
    }

    /**
     * ifMultiContains function, check if the player in the multiplayer exist in
     * the hash map
     *
     * @param name the name of the player
     * @return true if exist, else- false
     */
    public boolean ifMultiContains(String name) {
        return this.users.IfMultiContains(name);
    }

    /**
     * function addUser, add a User to the hash map
     *
     * @param user the user
     */
    public void addUser(User user) {
        this.users.AddUser(user);
    }

    /**
     * function addMultiUser, add a user to the multi hash map
     *
     * @param name the name of the user
     * @param user ths USer object
     */
    public void addMultiUser(String name, User user) {
        this.users.AddUserToMulti(name, user);
    }

    /**
     * this function check if the passwort matches to the username details
     *
     * @param name the name of the user
     * @param pass the password
     * @return true if matching, else- false
     */
    public boolean checkPassword(String name, String pass) {
        return this.users.CheckPassword(name, pass);
    }

    /**
     * send a message to the server
     *
     * @param name the name of the user that send the message
     * @param message the message
     */
    public void SendMessage(String name, String message) {
        this.users.getUser(name).sentToServer(message);
    }

    /**
     * receive a message from the server
     *
     * @param name the nemae of the user that recieve the message
     * @return the message
     */
    public String ReceiveMessage(String name) {
        return this.users.getUser(name).receiveFromServer();
    }

    /**
     * get the maze of the player
     *
     * @param username the username of the player
     * @return the atring of the maze
     */
    public String getMaze(String username) {
        return this.users.getUser(username).getMaze();
    }

}
