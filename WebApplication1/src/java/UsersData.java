
import java.util.HashMap;

/**
 * This class holds the information of the single and multiplayer games.
 *
 * @author Liron
 */
public class UsersData {

    //makes this class a singleton
    private static UsersData instance = null;
    //hold each signle player username with it's data
    private HashMap<String, User> map;
    //holds couples of multiplyers players
    private HashMap<String, User> multiMap;

    /**
     * class constructor.
     */
    protected UsersData() {
        map = new HashMap<String, User>();
        multiMap = new HashMap<String, User>();
    }

    /**
     * Make this class a singleton - only one instance.
     *
     * @return
     */
    public static UsersData GetInstance() {
        if (instance == null) {
            instance = new UsersData();
        }
        return instance;
    }

    /**
     * Add user to the single hash-map
     *
     * @param user
     */
    public void AddUser(User user) {
        this.map.put(user.GetUser(), user);
    }

    /**
     * check if a user is in the single hash map (in the game)
     *
     * @param name
     * @return true or false
     */
    public boolean IfContains(String name) {
        return (map.containsKey(name));
    }

    /**
     * check if a user is in the multiplayer hash map (in the game)
     *
     * @param name
     * @return
     */
    public boolean IfMultiContains(String name) {
        return (multiMap.containsKey(name));
    }

    /**
     * check if a user passwors is correct
     *
     * @param username
     * @param pass
     * @return true or false
     */
    public boolean CheckPassword(String username, String pass) {
        return (this.map.get(username).GetPassword().equals(pass));
    }

    /**
     * get a user details from the single hash-map using its username
     *
     * @param username
     * @return user
     */
    public User getUser(String username) {
        return this.map.get(username);
    }

    /**
     * add user to the multiplayer hash map
     *
     * @param name
     * @param user
     */
    public void AddUserToMulti(String name, User user) {
        this.multiMap.put(name, user);
    }

    /**
     * get a user details from the multiplayeer hash-map using its username
     *
     * @param name
     * @return user
     */
    public User getMultiUser(String name) {
        return this.multiMap.get(name);
    }
}
