
import java.io.IOException;
import java.io.Serializable;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Handles a connection with the server.
 */
@WebServlet(name = "GetJson", urlPatterns = {"/secured/GetJson"}, asyncSupported = true)
public class GetJson extends HttpServlet implements Serializable {

    /**
     * Create a thread the handles a connection with the server. Send a message
     * and receive a new maze information.
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException,
            IOException {
        //get the model object
        Model model = Model.GetInstance();
        //define asyncronic method
        final AsyncContext myContext = req.startAsync();
        myContext.setTimeout(0);
        //define the thread
        final Thread generator = new Thread() {
            @Override
            public void run() {
                try {
                    String jsonString;
                    HttpSession session = req.getSession();
                    String username = session.getAttribute("username").toString();
                    String mazeName = session.getAttribute("mazeName").toString();
                    //get maze's sizes
                    String width = getServletContext().getInitParameter("boardWidth");
                    String height = getServletContext().getInitParameter("boardHeight");
                    //id there is no maze for that player
                    if (model.getMaze(username).equals("")) {
                        //ask the server for a new maze
                        String message = "generate " + mazeName + " 0";
                        model.SendMessage(username, message);
                        //get th info and set it in the model
                        jsonString = model.ReceiveMessage(username);
                        model.getUser(username).setMaze(jsonString);
                        //if there is, get it
                    } else {
                        jsonString = model.getMaze(username);
                    }
                    //parse the info from the server
                    JSONParser parser = new JSONParser();
                    JSONObject obj = (JSONObject) parser.parse(jsonString);
                    obj.put("Width", width);
                    obj.put("Height", height);
                    obj.put("Photo", model.getUser(username).GetPhoto());
                    HttpServletResponse peer = (HttpServletResponse) myContext.getResponse();
                    peer.getWriter().write(obj.toString());
                    peer.setStatus(HttpServletResponse.SC_OK);
                    peer.setContentType("application/json");
                    myContext.complete();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        };
        //start the thread
        generator.start();
    }
}
