
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * the send hint message servlet
 */
@WebServlet(name = "SendHint", urlPatterns = {"/secured/SendHint"}, asyncSupported = true)
public class SendHint extends HttpServlet {

    @Override
    /**
     * the post function of the servlet
     *
     * @param req the request
     * @param resp the response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException,
            IOException {
        try {
            //gets the parameters
            String currentRow = req.getParameter("curR");
            String currentCol = req.getParameter("curC");
            String endRow = req.getParameter("endR");
            String endCol = req.getParameter("endC");
            //get the session
            HttpSession session = req.getSession();
            //get the mase details
            String mazeName = session.getAttribute("mazeName").toString();
            String username = session.getAttribute("username").toString();
            //create the message and send it
            String message = "solve " + mazeName + " 0 " + currentRow + " " + currentCol + " " + endRow + " " + endCol;
            Model model = Model.GetInstance();
            model.SendMessage(username, message);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
