
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
The main servlet of the "ask name" window (for multiplayer game).
 */
@WebServlet(name = "AskName", urlPatterns = {"/secured/AskName"})
public class AskName extends HttpServlet {

    /**
     * Display "AskName" page.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("askName.jsp");
    }

    /**
     * Create a multiplayer game. Send a request to the server for a maze, hadle
     * responde and go to multiplayer page.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mazeName = request.getParameter("mazeName");
        HttpSession session = request.getSession();
        String username = session.getAttribute("username").toString();
        session.setAttribute("mazeName", mazeName);
        Model model = Model.GetInstance();
        String message = "multiplayer " + mazeName;
        model.SendMessage(username, message);
        response.sendRedirect("/secured/Multiplayer");
    }
}
