
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * the single player servlet
 */
@WebServlet(name = "SinglePlayer", urlPatterns = {"/secured/SinglePlayer"})
public class SinglePlayer extends HttpServlet {

    /**
     * the get function of the servlet
     *
     * @param request the request
     * @param response the response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get the attributes of the session
        String name = request.getSession().getAttribute("name").toString();
        String photo = request.getSession().getAttribute("photo").toString();
        String mazeName = request.getSession().getAttribute("mazeName").toString();
        request.setAttribute("name", name);
        request.setAttribute("mazeName", mazeName);
        request.setAttribute("photo", photo);
        //open the jsp of the single player
        response.sendRedirect("singlePlayer.jsp");
    }

    /**
     * the post function of the single player
     *
     * @param request the request
     * @param response the response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //check if the player clicks on logout and move it to the login page
        if (request.getParameter("game").equals("logout")) {
            request.getSession().invalidate();
            response.sendRedirect("../login.jsp");
        }
    }
}
