
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * the send close message servlet
 */
@WebServlet(name = "SendClose", urlPatterns = {"/secured/SendClose"}, asyncSupported = true)
public class SendClose extends HttpServlet {

    /**
     * the post function of the servlet
     *
     * @param req the request
     * @param resp the response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException,
            IOException {
        try {
            //get the session
            HttpSession session = req.getSession();
            //get the attributes
            String username = session.getAttribute("username").toString();
            String mazeName = session.getAttribute("mazeName").toString();
            //create the message and send it
            String message = "close " + mazeName;
            Model model = Model.GetInstance();
            model.SendMessage(username, message);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
