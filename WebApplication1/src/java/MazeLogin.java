
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The main login servlet.
 */
@WebServlet(name = "MazeLogin", urlPatterns = {"/MazeLogin"})
public class MazeLogin extends HttpServlet {

    /**
     * The main "get" method. Direct the client to the login page.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }

    /**
     * Handle the login logic.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get the parameters entered in the screen
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Model model = Model.GetInstance();
        //check in the model if this username is valid
        if (model.checkUser(username)) {
            //if so, check the validity of the password
            if (model.checkPassword(username, password)) {
                //if valid - create a session for the user
                HttpSession session = request.getSession();
                //connect the session to the user's info
                session.setAttribute("photo", model.getUser(username).GetPhoto());
                session.setAttribute("username", username);
                session.setAttribute("name", model.getUser(username).GetName());
                response.sendRedirect("/secured/MazeMenu");
            }
            //if not - send error to the "jsp" file to handle in the screen
        } else {
            request.setAttribute("error", true);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}
