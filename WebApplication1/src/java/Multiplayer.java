
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is the multiplayer servlet
 */
@WebServlet(name = "Multiplayer", urlPatterns = {"/secured/Multiplayer"})
public class Multiplayer extends HttpServlet {

    /**
     * the multiplayer get, creates the page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get the attributes
        String mazeName = request.getSession().getAttribute("mazeName").toString();
        String name = request.getSession().getAttribute("name").toString();
        String username = request.getSession().getAttribute("username").toString();
        String photo = request.getSession().getAttribute("photo").toString();
        //set the attributes
        request.setAttribute("name", name);
        request.setAttribute("username", username);
        request.setAttribute("mazeName", mazeName);
        request.setAttribute("photo", photo);
        //open the jsp
        response.sendRedirect("multiplayer.jsp");
    }

    /**
     * the post function of the multiplayer
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //check if the user clicks on logout and go to the login page
        if (request.getParameter("game").equals("logout")) {
            request.getSession().invalidate();
            response.sendRedirect("../login.jsp");
        }
    }
}
