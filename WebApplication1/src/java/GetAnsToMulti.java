
import java.io.IOException;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * This class handles the communication between the server and the multiplayer.
 */
@WebServlet(name = "GetAnsToMulti", urlPatterns = {"/secured/GetAnsToMulti"}, asyncSupported = true)
public class GetAnsToMulti extends HttpServlet {

    /**
     * Handle multiplayer and server's communication threw thread.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException,
            IOException {
        final AsyncContext myContext = request.startAsync();
        myContext.setTimeout(0);
        //get the player's session
        final HttpSession session = request.getSession();
        //define a thread
        final Thread generator = new Thread() {
            @Override
            public void run() {
                try {
                    //get the message from the server
                    String username = session.getAttribute("username").toString();
                    Model model = Model.GetInstance();
                    String jsonString = model.ReceiveMessage(username);
                    //if it is not a "close" message - handle the multiplayer
                    if (!jsonString.equals("close")) {
                        JSONParser parser = new JSONParser();
                        JSONObject obj = (JSONObject) parser.parse(jsonString);
                        String type = obj.get("Type").toString();
                        //get the details
                        if (type.equals("3")) {
                            obj = getInfo(obj, request);
                        }
                        HttpServletResponse peer = (HttpServletResponse) myContext.getResponse();
                        peer.getWriter().write(obj.toString());
                        peer.setStatus(HttpServletResponse.SC_OK);
                        peer.setContentType("application/json");
                        myContext.complete();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        };
        //start the thread
        generator.start();
    }

    /**
     * Get a mupltiplayer details.
     *
     * @param obj
     * @param request
     * @return the details as a JSON object
     */
    public JSONObject getInfo(JSONObject obj, HttpServletRequest request) {
        Model model = Model.GetInstance();
        HttpSession session = request.getSession();
        String username = session.getAttribute("username").toString();
        String width = getServletContext().getInitParameter("boardWidth");
        String height = getServletContext().getInitParameter("boardHeight");
        //set as attributes
        JSONObject content = (JSONObject) obj.get("Content");
        JSONObject you = (JSONObject) content.get("You");
        String myName = you.get("Name").toString();
        model.addMultiUser(myName, model.getUser(username));
        JSONObject other = (JSONObject) content.get("Other");
        String otherName = other.get("Name").toString();
        obj.put("Width", width);
        obj.put("Height", height);
        obj.put("MyPhoto", model.getUser(username).GetPhoto());
        while (!model.ifMultiContains(otherName)) {
            continue;
        }
        obj.put("OtherPhoto", model.getMultiUser(otherName).GetPhoto());
        return obj;
    }
}
