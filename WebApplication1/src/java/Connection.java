
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.io.IOException;

/**
 * **********************************************************
 * The class creates a connection to the server.
 * **********************************************************
 */
public class Connection {

    private String ip;
    private int port;
    //private static Connection instance = null;
    private Socket socket;

    /**
     * Class constructor.
     *
     * @param port - server's port
     * @param ip - server's ip
     */
    protected Connection(int port, String ip) {
        this.ip = ip;
        this.port = port;
        //make the connection
        this.connect(this.ip, this.port);
    }

    /**
     * Getting the ip of the client.
     *
     * @return ip
     */
    public String getMyIp() {
        return this.ip;
    }

    /**
     * Setting the ip of the client.
     *
     * @param newIp - set this ip to class's member
     */
    public void setMyIp(String newIp) {
        this.ip = newIp;
    }

    /**
     * Getting the ip of the client.
     *
     * @return port
     */
    public int getMyPort() {
        return this.port;
    }

    /**
     * Setting the port of the client.
     *
     * @param newPort - set this port to class's member
     */
    public void setMyPort(int newPort) {
        this.port = newPort;
    }

    /**
     * Creates new socket and save it as a member.
     *
     * @param ip
     * @param port
     */
    public void connect(String ip, int port) {
        try {
            Socket sock = new Socket(ip, port);
            this.socket = sock;
        } catch (Exception e) {
            //handle exception
            System.out.println(e);
        }
    }

    /**
     * Get the message from the server.
     *
     * @return the message
     */
    public String receiveFromServer() {
        //create a buffer
        char[] buff = new char[4096];
        String fromServer = "";
        int size;
        //get the message
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            size = in.read(buff);
            fromServer = String.valueOf(buff, 0, size);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //return it
        return fromServer;
    }

    /**
     * Send a message to server.
     *
     * @param message - a string
     */
    public void sentToServer(String message) {
        try {
            PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true);
            if (message != null) {
                out.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
