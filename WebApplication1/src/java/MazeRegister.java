
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handle the registration.
 */
@WebServlet(name = "MazeRegister", urlPatterns = {"/MazeRegister"})
public class MazeRegister extends HttpServlet {

    /**
     * Direct the user to the registration page.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("register.jsp");
    }

    /**
     * Handle registration itself.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get the information the user has entered
        String username = request.getParameter("username");
        String pass = request.getParameter("password");
        String name = request.getParameter("name");
        String mail = request.getParameter("email");
        String photo = request.getParameter("photo");
        //if one of fields is empty, pass an error threw the "jsp" file to handle on screen 
        if (username.equals("") || pass.equals("") || name.equals("") || mail.equals("")) {
            request.setAttribute("error", true);
            request.setAttribute("username", username);
            request.setAttribute("email", mail);
            request.setAttribute("name", name);
            request.getRequestDispatcher("register.jsp").forward(request, response);
            //if not
        } else {
            //update the user info in the model
            Model model = Model.GetInstance();
            int port = Integer.parseInt(getServletContext().getInitParameter("port"));
            String ip = getServletContext().getInitParameter("ip");
            User user = new User(username, pass, name, mail, photo, port, ip);
            //check is this user already registered before
            if (!model.checkUser(username)) {
                //if not, add it to the model
                model.addUser(user);
                //if so, send a signal to the "jsp" file to handle on screen
            } else {
                request.setAttribute("exist", true);
                request.getRequestDispatcher("register.jsp").forward(request, response);
            }
            //get a session to the new user and send it's information forward
            HttpSession session = request.getSession();
            session.setAttribute("photo", model.getUser(username).GetPhoto());
            session.setAttribute("username", username);
            session.setAttribute("name", name);
            response.sendRedirect("/secured/MazeMenu");
        }
    }
}
