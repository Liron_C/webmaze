
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handle the initializtion of the single player game.
 *
 * @author Liron
 */
@WebServlet(name = "MazeMenu", urlPatterns = {"/secured/MazeMenu"})
public class MazeMenu extends HttpServlet {

    /**
     * Get all player's info and direct it to the menu screen.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getSession().getAttribute("username").toString();
        String name = request.getSession().getAttribute("name").toString();
        String photo = request.getSession().getAttribute("photo").toString();
        request.setAttribute("name", name);
        request.setAttribute("username", username);
        request.setAttribute("photo", photo);
        response.sendRedirect("menu.jsp");
    }

    /**
     * handle user pressing.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //check on which button the user pressed
        switch (request.getParameter("game")) {
            case "single":
                //get info and direct it to the single player page
                HttpSession session = request.getSession();
                session.setAttribute("mazeName", request.getSession().getAttribute("username").toString());
                response.sendRedirect("/secured/SinglePlayer");
                break;
            case "logout":
                //disconnect seesion and direct it to login page
                request.getSession().invalidate();
                response.sendRedirect("../login.jsp");
                break;
            case "multi":
                //direct it to "ask name" page
                request.getRequestDispatcher("askName.jsp").forward(request, response);
                break;
            default:
                break;
        }
    }
}
