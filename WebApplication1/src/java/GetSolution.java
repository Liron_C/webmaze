
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * This class handles the communication with the server for getting a hint.
 */
@WebServlet(name = "GetSolution", urlPatterns = {"/secured/GetSolution"}, asyncSupported = true)
public class GetSolution extends HttpServlet {

    /**
     * Get a hint for the player.
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException,
            IOException {
        try {
            //get all the needed information for the hint - received from the function caller
            HttpSession session = req.getSession();
            String username = session.getAttribute("username").toString();
            String currentRow = req.getParameter("curR");
            String currentCol = req.getParameter("curC");
            String endRow = req.getParameter("endR");
            String endCol = req.getParameter("endC");
            String mazeName = session.getAttribute("mazeName").toString();
            //create a message to send the server
            String message = "solve " + mazeName + " 0 " + currentRow + " " + currentCol + " " + endRow + " " + endCol;
            Model model = Model.GetInstance();
            model.SendMessage(username, message);
            //get the response
            String jsonString = model.ReceiveMessage(username);
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(jsonString);
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            //send the response forward
            resp.getWriter().write(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
